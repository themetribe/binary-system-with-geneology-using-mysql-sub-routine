-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 27, 2014 at 02:23 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inlight`
--

-- --------------------------------------------------------

--
-- Table structure for table `activation_codes`
--

CREATE TABLE IF NOT EXISTS `activation_codes` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned zerofill NOT NULL,
  `used_by_ID` int(10) unsigned zerofill NOT NULL,
  `date_created` datetime NOT NULL,
  `date_used` datetime NOT NULL,
  `code` varchar(6) NOT NULL,
  `status` int(1) NOT NULL COMMENT '1 : available | 0 : used',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geneology`
--

CREATE TABLE IF NOT EXISTS `geneology` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned zerofill NOT NULL,
  `child_left_ID` int(10) unsigned zerofill NOT NULL COMMENT 'from "user" table',
  `child_right_ID` int(10) unsigned zerofill NOT NULL COMMENT 'from "user" table',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='this must be inserted on "Sign Up" phase together with "users" table.' AUTO_INCREMENT=45 ;

--
-- Dumping data for table `geneology`
--

INSERT INTO `geneology` (`ID`, `user_ID`, `child_left_ID`, `child_right_ID`) VALUES
(1, 0000000001, 0000000002, 0000000003),
(2, 0000000002, 0000000050, 0000000000),
(3, 0000000003, 0000000000, 0000000000),
(34, 0000000050, 0000000051, 0000000052),
(35, 0000000051, 0000000054, 0000000000),
(36, 0000000052, 0000000000, 0000000000),
(37, 0000000053, 0000000000, 0000000000),
(38, 0000000054, 0000000055, 0000000056),
(39, 0000000055, 0000000057, 0000000058),
(40, 0000000056, 0000000059, 0000000060),
(41, 0000000057, 0000000000, 0000000000),
(42, 0000000058, 0000000000, 0000000000),
(43, 0000000059, 0000000000, 0000000000),
(44, 0000000060, 0000000000, 0000000000);

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE IF NOT EXISTS `library` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `date_uploaded` datetime NOT NULL,
  `excerpt` varchar(500) NOT NULL,
  `file_src` varchar(300) NOT NULL COMMENT 'path of file, this should be protected',
  `thumb_path` varchar(50) NOT NULL,
  `category` int(10) NOT NULL COMMENT '1: financial | 2:physical | 3:mental | 4:spiritual | 5:relationship',
  `type` int(1) NOT NULL COMMENT '1:ebook | 2:audio | 3:video',
  `mentor` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `library`
--

INSERT INTO `library` (`ID`, `title`, `date_uploaded`, `excerpt`, `file_src`, `thumb_path`, `category`, `type`, `mentor`) VALUES
(1, 'Ebook Sample', '2014-09-25 00:00:00', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum.', 'ebook-sample1.txt', 'book-sample.jpg', 5, 1, ''),
(2, 'Audio Sample', '2014-09-25 00:00:00', 'This is just an audio sample', 'audio-sample.txt', 'Selection_016.png', 1, 2, ''),
(3, 'Video Example', '2014-09-25 00:00:00', 'ssing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is there', 'video-sample.txt', 'Workspace 1_055.png', 3, 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned zerofill NOT NULL,
  `message` varchar(200) NOT NULL,
  `date_time` datetime NOT NULL,
  `from_ID` int(10) unsigned zerofill NOT NULL COMMENT 'fetched from "user" table',
  `type` int(1) NOT NULL COMMENT '0 : trash | 1 : inbox',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`ID`, `user_ID`, `message`, `date_time`, `from_ID`, `type`) VALUES
(1, 0000000002, 'Test', '2014-09-25 00:00:00', 0000000002, 1),
(2, 0000000002, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', '0000-00-00 00:00:00', 0000000002, 1),
(3, 0000000002, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distrib', '2014-09-25 05:25:53', 0000000002, 1);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `meta_name` varchar(100) NOT NULL,
  `meta_value` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='options holder' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `support_ticket`
--

CREATE TABLE IF NOT EXISTS `support_ticket` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) NOT NULL,
  `message` varchar(500) NOT NULL,
  `date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '0:pending | 1:active | 2:solved',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `type` int(5) NOT NULL COMMENT '1:membership | 2:upgrade | 3:widthraw | 4:purchase code',
  `date_transaction` datetime NOT NULL,
  `remarks` text NOT NULL,
  `reference` text NOT NULL,
  `gross` float NOT NULL,
  `date_sent` datetime NOT NULL COMMENT 'date from the form',
  `status` int(1) NOT NULL COMMENT '0 = pending | 1 = completed | 2 = declined',
  `payment_option` varchar(100) NOT NULL,
  `bank_branch` varchar(200) NOT NULL,
  `acc_no` int(20) NOT NULL,
  `acc_name` int(40) NOT NULL,
  `wire_name` varchar(200) NOT NULL COMMENT 'name of the person when using wire transfers',
  `wire_contact` varchar(200) NOT NULL,
  `wire_address` varchar(300) NOT NULL,
  `user_ID` int(10) NOT NULL,
  `receipt_picture` varchar(200) NOT NULL,
  `sponsor_ID` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`ID`, `type`, `date_transaction`, `remarks`, `reference`, `gross`, `date_sent`, `status`, `payment_option`, `bank_branch`, `acc_no`, `acc_name`, `wire_name`, `wire_contact`, `wire_address`, `user_ID`, `receipt_picture`, `sponsor_ID`) VALUES
(00001, 1, '2014-09-24 18:38:49', '', '45345345', 345, '2014-09-25 14:04:00', 0, 'BDO Bank', '3345345345345', 0, 0, '', '', '', 50, 'reg-5020140924183849.jpg', 2),
(00002, 1, '2014-09-25 15:55:11', '', '123123123', 123123000, '2014-09-25 15:07:00', 2, 'BPI Bank', '123123123123', 0, 0, '', '', '', 54, 'reg-5420140925155511.jpg', 51),
(00003, 1, '2014-09-25 15:58:47', '', '1231', 456, '2014-09-18 15:03:00', 1, 'BDO Bank', 'asdf', 0, 0, '', '', '', 54, 'reg-5420140925155847.jpg', 51);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `avatar_src` varchar(200) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `pin` int(6) NOT NULL,
  `sponsor_id` int(10) unsigned zerofill NOT NULL COMMENT 'ID of direct referrer/sponsor ("user" table)',
  `sposition` int(1) NOT NULL COMMENT 'Sponsore''s Position . options. 1 : left | 2: right',
  `status` int(1) NOT NULL COMMENT '0:pending | 1:active',
  `registered_date` datetime NOT NULL,
  `upgrade_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `upline_id` int(10) unsigned zerofill NOT NULL COMMENT 'ID of up-line ("user" table)',
  `uposition` int(1) NOT NULL COMMENT 'Upline''s Position. options : 1:left | 2:right',
  `account_type` int(5) NOT NULL COMMENT '1 : Basic | 2:Premier | 3:Silver | 4:Gold | 5:Diamond || company Side || 6: 01 | 7:admin',
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `bdate` varchar(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `island_group` varchar(50) NOT NULL,
  `atm_number` varchar(20) NOT NULL,
  `weekly_balance` varchar(10) NOT NULL DEFAULT '0',
  `monthly_balance` varchar(10) NOT NULL DEFAULT '0',
  `current_balance` varchar(10) NOT NULL DEFAULT '0',
  `total_balance` varchar(10) NOT NULL DEFAULT '0',
  `profit_share` varchar(10) NOT NULL DEFAULT '0',
  `personal_status` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `avatar_src`, `username`, `email`, `password`, `pin`, `sponsor_id`, `sposition`, `status`, `registered_date`, `upgrade_date`, `last_login`, `upline_id`, `uposition`, `account_type`, `fname`, `mname`, `lname`, `bdate`, `gender`, `phone`, `mobile`, `address`, `city`, `province`, `island_group`, `atm_number`, `weekly_balance`, `monthly_balance`, `current_balance`, `total_balance`, `profit_share`, `personal_status`) VALUES
(0000000001, 'primary-0000000001.jpg', 'kurt', '', '54e876eae95ebc3c47da1906313a31a5', 0, 0000000000, 1, 1, '2014-09-23 23:42:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000000, 1, 7, 'Kurt', '', 'Giger', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000002, 'primary-0000000002.jpg', 'jimmy', 'mrthemetribe@gmail.com', '3fa9c8995442a6f239628450dffc14e2', 5677, 0000000001, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000001, 1, 2, 'Jimmy', 'mid', 'Giger', '13/10/2011', 'Male', '123123123123', '', 'Maa Davao', 'Digos', 'Digos Prov', '', '', '100', '100', '100', '100', '3', 'Single'),
(0000000003, '', '01', '', '54e876eae95ebc3c47da1906313a31a5', 0, 0000000001, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000001, 2, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000050, '', 'quick', '', '52c8d7de1727a668e98ce18727c73785', 0, 0000000002, 1, 1, '2014-09-24 18:36:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000002, 1, 3, 'Quick', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000051, '', 'quick1', '', 'b55a3173bdd7c83251545bc5d7854d75', 0, 0000000050, 1, 1, '2014-09-24 18:36:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000050, 1, 2, 'Quick', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000052, '', 'quick2', '', '3471dbfe4d921f074ee8262425cedb34', 0, 0000000050, 2, 1, '2014-09-24 18:36:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000050, 2, 2, 'Quick', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000054, '', 'firstP', '', 'c792a1ea98f6347fa3898c2b12b85d0e', 0, 0000000051, 1, 1, '2014-09-25 15:48:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000051, 1, 2, 'First', 'k', 'P', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000055, '', 'firstP1', '', '0fd0fea3bdf275893cf492ca7391307a', 0, 0000000054, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000054, 1, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000056, '', 'firstP2', '', '0fd0fea3bdf275893cf492ca7391307a', 0, 0000000054, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000054, 2, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000057, '', 'firstP3', '', '0fd0fea3bdf275893cf492ca7391307a', 0, 0000000054, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000055, 1, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000058, '', 'firstP4', '', '0fd0fea3bdf275893cf492ca7391307a', 0, 0000000054, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000055, 2, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000059, '', 'firstP5', '', '0fd0fea3bdf275893cf492ca7391307a', 0, 0000000054, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000056, 1, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(0000000060, '', 'firstP6', '', '0fd0fea3bdf275893cf492ca7391307a', 0, 0000000054, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000056, 2, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
