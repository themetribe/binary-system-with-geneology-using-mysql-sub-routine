-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 24, 2014 at 05:15 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inlight`
--

-- --------------------------------------------------------

--
-- Table structure for table `activation_codes`
--

CREATE TABLE IF NOT EXISTS `activation_codes` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned zerofill NOT NULL,
  `used_by_ID` int(10) unsigned zerofill NOT NULL,
  `date_created` datetime NOT NULL,
  `date_used` datetime NOT NULL,
  `code` varchar(6) NOT NULL,
  `status` int(1) NOT NULL COMMENT '1 : available | 0 : used',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geneology`
--

CREATE TABLE IF NOT EXISTS `geneology` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned zerofill NOT NULL,
  `child_left_ID` int(10) unsigned zerofill NOT NULL COMMENT 'from "user" table',
  `child_right_ID` int(10) unsigned zerofill NOT NULL COMMENT 'from "user" table',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='this must be inserted on "Sign Up" phase together with "users" table.' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `geneology`
--

INSERT INTO `geneology` (`ID`, `user_ID`, `child_left_ID`, `child_right_ID`) VALUES
(1, 0000000001, 0000000002, 0000000003),
(2, 0000000002, 0000000000, 0000000000),
(3, 0000000003, 0000000000, 0000000000);

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE IF NOT EXISTS `library` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `date_uploaded` datetime NOT NULL,
  `excerpt` varchar(500) NOT NULL,
  `file_src` varchar(300) NOT NULL COMMENT 'path of file, this should be protected',
  `thumb_path` varchar(50) NOT NULL,
  `category` int(10) NOT NULL COMMENT '1: financial | 2:physical | 3:mental | 4:spiritual | 5:relationship',
  `type` int(1) NOT NULL COMMENT '1:ebook | 2:audio | 3:video',
  `mentor` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned zerofill NOT NULL,
  `message` varchar(200) NOT NULL,
  `date_time` datetime NOT NULL,
  `from_ID` int(10) unsigned zerofill NOT NULL COMMENT 'fetched from "user" table',
  `type` int(1) NOT NULL COMMENT '0 : trash | 1 : inbox',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `meta_name` varchar(100) NOT NULL,
  `meta_value` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='options holder' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `support_ticket`
--

CREATE TABLE IF NOT EXISTS `support_ticket` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) NOT NULL,
  `message` varchar(500) NOT NULL,
  `date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '0:pending | 1:active | 2:solved',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `ID` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `type` int(5) NOT NULL COMMENT '1:membership | 2:upgrade | 3:widthraw | 4:purchase code',
  `date_transaction` datetime NOT NULL,
  `remarks` text NOT NULL,
  `reference` text NOT NULL,
  `gross` float NOT NULL,
  `date_sent` datetime NOT NULL COMMENT 'date from the form',
  `status` int(1) NOT NULL COMMENT '0 = pending | 1 = completed | 2 = declined',
  `payment_option` varchar(100) NOT NULL,
  `bank_branch` varchar(200) NOT NULL,
  `acc_no` int(20) NOT NULL,
  `acc_name` int(40) NOT NULL,
  `wire_name` varchar(200) NOT NULL COMMENT 'name of the person when using wire transfers',
  `wire_contact` varchar(200) NOT NULL,
  `wire_address` varchar(300) NOT NULL,
  `user_ID` int(10) NOT NULL,
  `receipt_picture` varchar(200) NOT NULL,
  `sponsor_ID` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `avatar_src` varchar(200) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `pin` int(6) NOT NULL,
  `sponsor_id` int(10) unsigned zerofill NOT NULL COMMENT 'ID of direct referrer/sponsor ("user" table)',
  `sposition` int(1) NOT NULL COMMENT 'Sponsore''s Position . options. 1 : left | 2: right',
  `status` int(1) NOT NULL COMMENT '0:pending | 1:active',
  `registered_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `upline_id` int(10) unsigned zerofill NOT NULL COMMENT 'ID of up-line ("user" table)',
  `uposition` int(1) NOT NULL COMMENT 'Upline''s Position. options : 1:left | 2:right',
  `account_type` int(5) NOT NULL COMMENT '1 : Basic | 2:Premier | 3:Silver | 4:Gold | 5:Diamond || company Side || 6: 01 | 7:admin',
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `bdate` varchar(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `address` varchar(200) NOT NULL,
  `island_group` varchar(50) NOT NULL,
  `atm_number` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `avatar_src`, `username`, `email`, `password`, `pin`, `sponsor_id`, `sposition`, `status`, `registered_date`, `last_login`, `upline_id`, `uposition`, `account_type`, `fname`, `mname`, `lname`, `bdate`, `gender`, `phone`, `mobile`, `address`, `island_group`, `atm_number`) VALUES
(0000000001, 'primary-0000000001.jpg', 'kurt', '', '54e876eae95ebc3c47da1906313a31a5', 0, 0000000000, 1, 1, '2014-09-23 23:42:20', '0000-00-00 00:00:00', 0000000000, 1, 7, 'Kurt', '', 'Giger', '', 'male', '', '', '', '', ''),
(0000000002, '', 'jimmy', '', '', 0, 0000000001, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000001, 1, 6, 'Jimmy', '', 'Giger', '', '', '', '', '', '', ''),
(0000000003, '', '01', '', '', 0, 0000000001, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0000000001, 2, 6, '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE IF NOT EXISTS `user_meta` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `user_ID` int(5) NOT NULL,
  `meta_name` varchar(100) NOT NULL,
  `meta_value` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Should include 0 to * user attrs ( payment_details, account_num, etc ) ' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
