-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2014 at 12:20 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inlight`
--

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE IF NOT EXISTS `library` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `date_uploaded` datetime NOT NULL,
  `excerpt` varchar(500) NOT NULL,
  `file_src` varchar(300) NOT NULL COMMENT 'path of file, this should be protected',
  `thumb_path` varchar(50) NOT NULL,
  `category` int(10) NOT NULL COMMENT '1: financial | 2:physical | 3:mental | 4:spiritual | 5:relationship',
  `ebook_category` varchar(100) NOT NULL,
  `type` int(1) NOT NULL COMMENT '1:ebook | 2:audio | 3:video',
  `mentor` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `library`
--

INSERT INTO `library` (`ID`, `title`, `date_uploaded`, `excerpt`, `file_src`, `thumb_path`, `category`, `ebook_category`, `type`, `mentor`) VALUES
(1, 'Ebook Sample', '2014-09-25 00:00:00', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum.', 'ebook-sample1.txt', 'book-sample.jpg', 5, 'Beauty and Fashion', 1, ''),
(2, 'Audio Sample', '2014-09-25 00:00:00', 'This is just an audio sample', 'audio-sample.txt', 'Selection_016.png', 1, '', 2, ''),
(3, 'Video Example', '2014-09-25 00:00:00', 'ssing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is there', 'video-sample.txt', 'Workspace 1_055.png', 3, '', 3, ''),
(4, 'Resume Version 1', '2014-09-25 00:00:00', 'Mauris in feugiat metus, id ullamcorper mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus vel orci nec metus tincidunt molestie at sit amet neque. Phasellus non consequat urna. Nunc vel eros finibus, convallis orci at, blandit quam. In molestie leo sit amet arcu congue, vitae porta nibh lacinia. Vivamus ultrices dui eget finibus congue. Vivamus posuere dapibus mollis. Quisque leo dui, cursus et lacus volutpat, ornare imperdiet neque. Fusce ', 'FRANCIS_MAYOLA_ALBORES_2013.doc', 'resume-doc.jpg', 5, 'Biography', 1, ''),
(5, 'Resume of FMA', '2014-09-25 00:00:00', 'Mauris in feugiat metus, id ullamcorper mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus vel orci nec metus tincidunt molestie at sit amet neque. Phasellus non consequat urna. Nunc vel eros finibus, convallis orci at, blandit quam. In molestie leo sit amet arcu congue, vitae porta nibh lacinia. Vivamus ultrices dui eget finibus congue. Vivamus posuere dapibus mollis. Quisque leo dui, cursus et lacus volutpat, ornare imperdiet neque. Fusce ', 'FRANCIS_MAYOLA_ALBORES_2014.pdf', 'resume-pdf.jpg', 3, 'Biography', 1, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
