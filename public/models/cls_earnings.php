<?php
class Earnings extends DB{
	public function __construct(){
		parent::__construct(); 
	}
	public function calculate(){
		$data['current']=0;
		$dp = $this->dref_points();
		$pp = $this->pair_points();		
		$data['total']= ($dp + $pp['pairing']) * POINT_VALUE;

		$dp = $this->dref_points('weekly');
		$pp = $this->pair_points('weekly');
		$data['weekly']= ($dp + $pp['pairing']) * POINT_VALUE;

		$dp = $this->dref_points('monthly');
		$pp = $this->pair_points('monthly');
		$data['monthly']= ($dp + $pp['pairing']) * POINT_VALUE;

		$totalWidthrawals = $this->getTotalWidthrawals();		
		$data['current'] = $data['total'] - $totalWidthrawals;

		switch($_SESSION['user_type']){
			case 3 : $data['investor_share'] = $data['total']*(SILVER_SHARE/100);  break;
			case 4 : $data['investor_share'] = $data['total']*(GOLD_SHARE/100);  break;
			case 5 : $data['investor_share'] = $data['total']*(DIAMOND_SHARE/100);  break;
		}
		return $data;
	}

	private function getTotalWidthrawals(){
		$q = "	SELECT SUM( gross ) as total_widthrawal 
				FROM transaction
				WHERE type =  :type
				AND user_ID =  :user_ID AND status = :status";
		$arr = array("type"=>3, "user_ID"=>$_SESSION['user_ID'], "status"=>1);
		$db = $this->query($q,$arr);
		$return = $db->fetchAll(PDO::FETCH_ASSOC);
		return abs($return[0]['total_widthrawal']);
	}
	private function dref_points($type = ""){
		$premier_where = array(
			"sponsor_ID"=>$_SESSION['user_ID'],
			"account_type"=>2
			);	

		$basic_where = array(
			"sponsor_ID"=>$_SESSION['user_ID'],
			"account_type"=>1
			);
		if($type!=""){
			switch($type){
				case "weekly":
					$q ="	SELECT COUNT(*) AS c
							FROM user 
							WHERE sponsor_ID = :sponsor_ID
							AND account_type = :account_type
							AND status=1
							AND ( registered_date 
								BETWEEN ( DATE_SUB( NOW() , INTERVAL 7+WEEKDAY(NOW()) DAY ) ) 
								AND ( DATE_SUB( NOW() , INTERVAL 1+WEEKDAY(NOW()) DAY ) )  
							)
							";			
					break;
				case "monthly":
					$q ="	SELECT COUNT(*)  AS c
							FROM user 
							WHERE sponsor_ID = :sponsor_ID
							AND account_type = :account_type
							AND status=1
							AND ( registered_date BETWEEN ( LAST_DAY(NOW() - INTERVAL 1 MONTH) + INTERVAL 1 DAY ) AND ( LAST_DAY(NOW()) ) )
							";
					break;
			}


			$db = $this->query($q,$premier_where);
			$pdr = $db->fetchAll(PDO::FETCH_ASSOC);
			$db = $this->query($q,$basic_where);
			$bdr = $db->fetchAll(PDO::FETCH_ASSOC);			
		}
		else{
			$premier_where = array(
				"sponsor_ID"=>$_SESSION['user_ID'],
				"account_type_premier"=>2,
				"account_type_silver"=>3,
				"account_type_gold"=>4,
				"account_type_diamond"=>5
				);	

			$basic_where = array(
				"sponsor_ID"=>$_SESSION['user_ID'],
				"account_type"=>1,
				'status'=>1
				);

			$q = "	SELECT COUNT(*) AS c 
					FROM user 
					WHERE sponsor_ID = :sponsor_ID 
					AND status = 1
					AND (   account_type = :account_type_premier 
						OR 	account_type = :account_type_silver
						OR 	account_type = :account_type_gold
						OR 	account_type = :account_type_diamond )";
			$db = $this->query($q,$premier_where);
			$pdr = $db->fetchAll(PDO::FETCH_ASSOC);
			$bdr = $this->select("COUNT(*) AS c",$basic_where,false, "user");
		}
		return $pdr[0]['c'] + $bdr[0]['c'];
	}

	private function pair_points($type = ""){
		// LEFT
		$where = array(
				"account_type"=>2,
				"sponsor_ID"=>$_SESSION['user_ID'],
				"sposition"=>1
			);
		$q1 = $this->select("COUNT(*) AS c",$where, true, "user");
		$where = array(
				"account_type"=>1,
				"sponsor_ID"=>$_SESSION['user_ID'],
				"sposition"=>1
			);
		$q2 = $this->select("COUNT(*) AS c",$where, true, "user");
		$lp = abs( $q1['c'] ) + abs($q2['c']);

		// RIGHT
		$where = array(
				"account_type"=>2,
				"sponsor_ID"=>$_SESSION['user_ID'],
				"sposition"=>2
			);
		$q1 = $this->select("COUNT(*) AS c",$where, true, "user");		
		$where = array(
				"account_type"=>1,
				"sponsor_ID"=>$_SESSION['user_ID'],
				"sposition"=>2
			);
		$q2 = $this->select("COUNT(*) AS c",$where, true, "user");
		$rp = abs( $q1['c'] ) + abs($q2['c']);
		$data['pairing']= ($lp<$rp) ? $lp : $rp;
		$data['waiting']= ($lp+$rp) - $data['pairing'];

		return $data;

	}
}
?>