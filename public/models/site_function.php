<?php
class Func{
	public static function body_class($cls){
		$string = "class=\"$cls ";
		// DETECT PAGE
		if(isset($_GET['page']))
			$string.=$_GET['page']." ";

		// DETECT BROWSER
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10')!==false) $browser = "ie10";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9')!==false) $browser = "ie9";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8')!==false) $browser = "ie8";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7')!==false) $browser = "ie7";	
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox')!==false) $browser = "firefox";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'Safari')!==false) $browser = "safari";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')!==false) $browser = "chrome";

		$string.=$browser." ";		
		$string = substr($string, 0,-1);
		$string .='"';
		echo $string;
	}
	public static function init(){
		global $function_name;
		$function_name = '';	
	} 
	public static function footer_hook($_func_name){
		global $function_name;
		$function_name = $_func_name;
	}
	public static function execute_footer_hook(){
		global $function_name;
		if(function_exists($function_name))
			$function_name();
	}
	public static function loader_modal(){ ?>
		<div class="modal fade" id="loader_modal"> 
			<div class="modal-dialog">
				<img src="<?php echo SITE_URL ?>/assets/img/loader.gif" />
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<?php
	}	

	public static function generate_secured_img_src($Get){
		switch($Get['t']){
			case 'reg':

				$file = 'file://'.UPLOADS_DIR."/reg-{$Get['img']}.{$Get['ext']}";
				$type = 'image/jpeg';
				header('Content-Type:'.$type);
				header('Content-Length: ' . filesize($file));
				readfile($file);
				break;
			case 'ebookthumb':
				$file = 'file://'.RESOURCE_DIR."/ebooks/thumb/{$Get['img']}";
				$type = 'image/jpeg';
				header('Content-Type:'.$type);
				header('Content-Length: ' . filesize($file));
				readfile($file);
				break;

			case 'audiothumb':

				$file = 'file://'.RESOURCE_DIR."/audios/thumb/{$Get['img']}";
				$type = 'mp3/mp4';
				header('Content-Type:'.$type);
				header('Content-Length: ' . filesize($file));
				readfile($file);
				break;

			case 'videothumb':

				$file = 'file://'.RESOURCE_DIR."/videos/thumb/{$Get['img']}";
				$type = 'ogg/mp3';
				header('Content-Type:'.$type);
				header('Content-Length: ' . filesize($file));
				readfile($file);
				break;

			case 'ebook':
				$file = 'file://'.RESOURCE_DIR."/ebooks/{$Get['img']}";
				$type = 'text/plain';
				header('Content-Disposition: attachment; filename="'.$Get['img'].'"');
				header('Content-Type:'.$type);				
				readfile($file);
				exit();
				break;
			case 'audio':
				$file = 'file://'.RESOURCE_DIR."/audios/{$Get['img']}";
				$type = 'text/plain';
				header('Content-Disposition: attachment; filename="'.$Get['img'].'"');
				header('Content-Type:'.$type);				
				readfile($file);
				exit();
				break;
			case 'video':
				$file = 'file://'.RESOURCE_DIR."/videos/{$Get['img']}";
				$type = 'text/plain';
				header('Content-Disposition: attachment; filename="'.$Get['img'].'"');
				header('Content-Type:'.$type);				
				readfile($file);
				exit();
				break;
			case "primary": //avatar primary photo
				$file = 'file://'.UPLOADS_DIR."{$Get['img']}.{$Get['ext']}";				
				$type = 'image/jpeg';
				header('Content-Type:'.$type);
				header('Content-Length: ' . filesize($file));
				readfile($file);
				break;
		}
		//return $data;
	}
	public static function inactive_blocker(){
		if($_SESSION['user_status']==0)
			header('Location:myaccount');
	}
	public static function get_secured_image($image_src,$display=true,$pre="reg"){
		$image_src = str_replace('reg-', '', $image_src);
		$img = explode('.', $image_src);
		$image_src = str_replace('.'.end($img), '', $image_src);
		if(!$display)
			return SITE_URL."/dashboard?t={$pre}&img=".$image_src."&ext=".end($img);		
		else
			echo SITE_URL."/dashboard?t={$pre}&img=".$image_src."&ext=".end($img);
	}

	public static function get_secured_resources($type, $image_src,$display=true){
		
		if(!$display)
			return SITE_URL."/dashboard?t=".$type."&img=".$image_src;		
		else
			echo SITE_URL."/dashboard?t=".$type."&img=".$image_src;
	}

	public static function transaction_outputs($type){		
		ob_start();
		?>
		<option value="">--[ Choose Payment Option ]--</option>
		<option class="bank" data-details="bpi-details">BPI Bank</option>
		<option class="bank" data-details="bdo-details">BDO Bank</option>
		<option class="bank" data-details="unionbank-details">UnionBank</option>
		<option class="wire" data-details="lbc-details">LBC</option>
		<option class="wire" data-details="cebuana-details">Cebuana</option>
		<option class="wire" data-details="palawan-exp-details">Palawan Exp</option>
		<option class="wire" data-details="mlh-details">MLHuillier</option>
		<option class="wire" data-details="westernunion-details">Western Union</option>
		<option class="wire" data-details="moneygram-details">Moneygram</option>
		<option class="bank" data-details="rcbc-details">RCBC Bank</option>
		<option class="wire" data-details="rd-details">RD Pawnshop</option>
		<?php
		$data = ob_get_contents();
		ob_end_clean();

		if($type=="js"){
			$data = str_replace(array("<option","</option>"), array("'<option","</option>'+"), $data);
		}
		echo  $data;
	}

	public static function random_string($length) {
	    $key = '';
	    $keys = array_merge(range(0, 9), range('a', 'z'));

	    for ($i = 0; $i < $length; $i++) {
	        $key .= $keys[array_rand($keys)];
	    }

	    return $key;
	}

	public function investor_share_percentage(){
		if($_SESSION['user_type']==3)
			return SILVER_SHARE;
		elseif($_SESSION['user_type']==4)
			return GOLD_SHARE;
		elseif($_SESSION['user_type']==5)
			return DIAMOND_SHARE;
	}
}

?>