<?php
session_start();

// Calculating Earnings upon logging into the system
if(isset($_POST['a'])){
	/*
	MANUAL FOR NOW
	switch($_POST['a']){
		case "calculate_earnings": 
			include("models/cls_earnings.php");
			$earn = new Earnings();
			$money = $earn->calculate();
			$_SESSION['total'] = $money['total'];
			$_SESSION['weekly'] = $money['weekly'];
			$_SESSION['monthly'] = $money['monthly'];
			$_SESSION['current'] = $money['current'];

			//  INVESTORS
			if($_SESSION['user_type']==3 || $_SESSION['user_type']==4 || $_SESSION['user_type']==5)
				$_SESSION['investor_share'] = $money['investor_share'];
			echo 1;
		break;
	}
	*/
	echo 1;
	exit();
}

// Securing Images - reason why it is here, because this will only be accessible if session is passed, which makes it more secure
if(isset($_GET['img'])){
	Func::generate_secured_img_src($_GET);
}
include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");



if(isset($_GET['logged'])) :
	include("views/admin/welcome.php");

	function script() { ?>
		<script>
			var _data = "a=calculate_earnings";
			$.post(window.location.href,_data, function(data){
				if($.trim(data)==1){
					$("#welcome").fadeOut(9000);
					setTimeout(function(){
						window.location.href='/dashboard';
					},5000);
				}
				console.log(data);
			});			
		</script>
		<?php
	}

else:

	function script() { ?>
		<script type="text/javascript" src="http://malsup.github.com/jquery.cycle.all.js"></script>
		<script>
			(function(global, $){ $(document).ready(function(){
				Dashboard.listener($);
				Dashboard.slideshow();
			}); })(window, jQuery);
			var Dashboard = {
				that : null, $ : null,
				listener : function($){
					that=this; $ = $;
				},
				slideshow : function(){
					$('.slideshow').cycle({
						fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
					});
				}
			}


		</script>

		<?php
	}
	

	include("views/admin/dashboard.php");	

endif;

Func::footer_hook('script');

include("views/partials/admin_footer.php");