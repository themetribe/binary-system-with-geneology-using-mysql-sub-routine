<?php
session_start();
Func::inactive_blocker();

include("models/cls_geneology.php");
$geneology = new Geneology();
//$graph_data = $geneology->get_affiliates();
//print_r($graph_data);

#if(isset($_POST['a']) && $_POST['a']=="fetchdownlines"){

#}

include("models/cls_message.php");
$message = new Message();

$inbox = $message->fetchInbox();
$trash = $message->fetchTrash();
$sent = $message->fetchSent();

include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");
include("views/admin/messages.php");

function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Messages.listener($);

		}); })(window, jQuery);
		var Messages = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				this.viewDetails($);
				/* for future 
				$("#btn-compose-message").on('click',function(){
					
					Messages.loadSentToOptions();
					that.show_compose();
				})
				*/
				
			},
			viewDetails : function($){
				$(".view-details").on('click',function(){
					Modal.hasButton = false;
					Modal._title = "Messaging Details";
					Modal.addId = "messaging-tips";
					Modal.contents = 
						'<h1>"'+$(this).data('fullmessage')+'"</h1>'+
						'<p> - '+$(this).parents('tr').find('.from').html()+'</p>';
					Modal.show($);
					return false;
				})
			}



			/* for future,
			show_compose : function(){
				Modal.hasHeader = 
				Modal.hasButton = false;
				Modal.addId = "compose_message_modal";
				Modal.contents = 
					'<form role="form">' +
						'<div class="form-group">'+
							'<label for="">To:</label>'+
							'<select id="sentto" name="sentto">'+
								'<option value="">--[choose]--</option>'+
							'</select>'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Messages:</label>'+
							'<textarea class="form-control" placeholder=""></textarea>'+
						'</div>'+
						'<a class="btn btn-primary" href="#">Submit Message</a>'+
					'</form>'
				Modal.show($);
			}
			loadSentToOptions : function(){
				show_loader($);
				var _data = "a=fetchdownlines";
				$.post(window.location.href,_data, function(data){
					data = $.trim(data);
					if(data!=-1){								
						
					}
					close_loader($);
					console.log(data);
				});
			}*/
		}		
	</script>
<?php
}
Func::footer_hook('script');
include("views/partials/admin_footer.php");
