<?php
session_start();
if(isset($_GET['logout'])){	
	session_destroy();
	header('location:'.SITE_URL);	
}
if(isset($_POST['a'])){
	include("models/cls_user.php");
	$user = new User();
	echo $user->login($_POST);
	exit();
}

include("models/cls_support.php");
$support = new Support();

if(isset($_POST['btn-send'])){
	$support->send($_POST['email']);
	$sent = true;
}

include("views/partials/html_head.php");
include("views/home.php");


function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Home.listener($);
			<?php if(isset($_GET['logout']) && $_GET['logout']==2) : ?>
			Home.show_login();
			<?php endif; ?>

			header.scroll($);

		}); })(window, jQuery);
		var header = {
			scroll : function($){
				$(window).scroll(function(){
					if ($(window).scrollTop() >= ($("#menu").offset().top)) {
			            $("#menu").addClass("has-skin");
			            $("#menu").find("a").removeClass("btn btn-default");
			        }
			        if($(window).scrollTop() <= ($("#about-us").offset().top)){
			        	$("#menu").removeClass("has-skin");
			        	$("#menu").find("a").addClass("btn btn-default");
			        }
				})
			}
		}
		var Home = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$("#btn_get-started").on('click',function(){
					that.show_menus(this);
				})
				$("#btnlogin").on('click',function(){
					that.show_login(this);
				});
				$(document).on('submit',"#member_login form",function(event){
					event.preventDefault();
					that.login_request(this);
					return false;
				})
			},
			show_menus : function(_this){
				
			},
			show_login : function(_this){
				Modal._title = "Affiliate Login"
				Modal.hasButton = false;
				Modal.addId = "member_login";
				Modal.contents =
					'<form role="form" method="POST">'+
					'	<div class="form-group">'+
					'		<label for="">Username</label>'+
					'		<input type="text" class="form-control" id="username" name="username" placeholder="">'+
					'	</div>'+
					'	<div class="form-group">'+
					'		<label for="">Password</label>'+
					'		<input type="password" class="form-control" id="password" name="password" placeholder="">'+
					'	</div>'+
					'	<input type="submit" class="btn btn-primary pull-right" value="Login" />'+
					'</form>';
				Modal.show($);
			},
			login_request : function(_this){
				var _data = "a=1&"+$(_this).serialize();;
				$.post(window.location.href,_data, function(data){
					if($.trim(data)==1){
						window.location.href="/dashboard?logged=1";
					}
					else { // if($.trim(data)==2){
						alert('Sorry your account is invalid.');
						$("#password").val("").focus();
					}
					close_loader($);
					console.log(data);
				});
			}
		}		
	</script>
	<?php
}
Func::footer_hook('script');
include("views/partials/html_foot.php");
?>