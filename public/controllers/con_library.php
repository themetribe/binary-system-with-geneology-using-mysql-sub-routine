<?php
session_start();
Func::inactive_blocker();
include("models/cls_library.php");
$cls_library = new Library();
if(isset($_GET['t'])){
	switch($_GET['t']){
		case 1: // ebook
			$library = $cls_library->getebooks_lists($_GET['cat']); 
			break;
		case 2: //audio
			$library = $cls_library->getaudio_lists($_GET['cat']); 
			break;
		case 3: //video
			$library = $cls_library->getvideo_lists($_GET['cat']); 
			break;
	}
}
else{
	$library = $cls_library->getall(); 
}
 

include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");
include("views/admin/library.php");



function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Library.listener($);
		}); }) (window, jQuery);

		var Library = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$(document).on('click',".contents .item",function(){

					var thumb = $(this).find('img').attr('src');
					var title = $(this).data('title');
					var excerpt =$(this).data('excerpt');
					var filesrc= $(this).data('link');

					Modal.hasHeader = 
					Modal.hasButton = false;
					Modal.addId = "product_detail_modal";
					Modal.contents =
						'<div class="product-image"> '+
						'	<img width="215" src="'+thumb+'" />'+
						'</div>'+
						'<div class="product-description">'+
						'	<h3>'+title+'</h3>'+
						'	<p>'+excerpt+'</p>'+
						'	<a href="'+filesrc+'" class="btn btn-primary btn-lg">Download</a>'+
						'</div>'; /* HIDE FOR NOW +
						'<a href="#" class="btn btn-default"><i class="fa fa-star"></i> Add To Favorite</a>';*/
					
					Modal.show($);

					$("#product_detail_modal").on('hidden.bs.modal', function (e) {
						console.log('remove');
					  $("#product_detail_modal").remove();
					})
					
				});

				$('.dropdown-toggle').dropdown();

				$("#menu .ebooks").on('click',function(){
					that.ebooksModal();
					return false;
				});

				$("#menu .video").on('click',function(){
					that.videoModal();
					return false;
				});

				$("#menu .audio").on('click',function(){
					that.audioModal();
					return false;
				});

				$(document).on('click',"#category_submodal a",function(e){
					e.preventDefault(); 
					var goTo=$(this).attr("href");
					show_loader($,"#category_submodal");
					setTimeout(function(){
						window.location.href = goTo;
					},900);
					return false;
				})
			},
			audioModal : function(){
				Modal._title = "Audio Categories";
				Modal.hasButton = false;
				Modal.hasHeader = true;
				Modal.addId = "category_submodal";
				Modal.contents = '<p>Vivamus consectetur nisi a dapibus finibus. Integer a tristique risus, sit amet dapibus tellus. Maecenas fermentum nunc dui, eget congue nulla luctus in. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>'+
					'<br /><div class="row">'+
					'	<div class="col-md-6">'+
					'		<ul>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=2&cat=1">Financial</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=2&cat=2">Physical</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=2&cat=3">Mental</a></li>'+
					'		</ul>'+
					'	</div>'+
					'	<div class="col-md-6">'+
					'		<ul>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=2&cat=4">Spiritual</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=2&cat=5">Relationship</a></li>'+
					'		</ul>'+
					'	</div>'+
					'</div>';
				Modal.show($);
				$("#"+Modal.addId).on('hidden.bs.modal', function (e) {
					console.log('remove');
				  $("#"+Modal.addId).remove();
				})
			},
			videoModal : function(){
				Modal._title = "Video Categories";
				Modal.hasButton = false;
				Modal.hasHeader = true;
				Modal.addId = "category_submodal";
				Modal.contents = '<p>Vivamus consectetur nisi a dapibus finibus. Integer a tristique risus, sit amet dapibus tellus. Maecenas fermentum nunc dui, eget congue nulla luctus in. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>'+
					'<br /><div class="row">'+
					'	<div class="col-md-6">'+
					'		<ul>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=3&cat=1">Financial</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=3&cat=2">Physical</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=3&cat=3">Mental</a></li>'+
					'		</ul>'+
					'	</div>'+
					'	<div class="col-md-6">'+
					'		<ul>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=3&cat=4">Spiritual</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=3&cat=5">Relationship</a></li>'+
					'		</ul>'+
					'	</div>'+
					'</div>';
				Modal.show($);
				$("#"+Modal.addId).on('hidden.bs.modal', function (e) {
					console.log('remove');
				  $("#"+Modal.addId).remove();
				})
			},
			ebooksModal : function(){
				Modal._title = "Ebooks Categories";
				Modal.hasButton = false;
				Modal.hasHeader = true;
				Modal.addId = "category_submodal";
				Modal.contents = '<p>Vivamus consectetur nisi a dapibus finibus. Integer a tristique risus, sit amet dapibus tellus. Maecenas fermentum nunc dui, eget congue nulla luctus in. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>'+
					'<br /><div class="row">'+
					'	<div class="col-md-6">'+
					'		<ul>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Beauty and Fashion">Beauty and Fashion</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Biography">Biography</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Body and Mind">Body and Mind</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Business and Financial">Business and Financial</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Career">Career</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Communication">Communication</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Computer and Internet">Computer and Internet</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Drama">Drama</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Educational">Educational</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Environment">Environment</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Fiction">Fiction</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Fitness">Fitness</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Food and Recipes">Food and Recipes</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Games">Games</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Health">Health</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=History">History</a></li>'+
					'		</ul>'+
					'	</div>'+
					'	<div class="col-md-6">'+
					'		<ul>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Horror">Horror</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Humanities">Humanities</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Humor">Humor</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Internet Marketing">Internet Marketing</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Magazines">Magazines</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Network Marketing">Network Marketing</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Parenting and Children">Parenting and Children</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Personal Development">Personal Development</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Politics">Politics</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Religious">Religious</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Science">Science</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Short Stories">Short Stories</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Technology">Technology</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Travel">Travel</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=Tutorials">Tutorials</a></li>'+
					'			<li><a href="<?php echo SITE_URL ?>/library?t=1&cat=">All</a></li>'+
					'		</ul>'+
					'	</div>'+
					'</div>';
				Modal.show($);
				$("#"+Modal.addId).on('hidden.bs.modal', function (e) {
					console.log('remove');
				  $("#"+Modal.addId).remove();
				})
			}


		}
	</script>
	<?php	
}
Func::footer_hook('script');
include("views/partials/admin_footer.php");