<?php
session_start();
Func::inactive_blocker();

if(isset($_POST['a']) && $_POST['a']=="sendmessage"){
	include("models/cls_message.php");	
	$message = new Message();
	$message->toInbox($_POST);
	exit();
}

include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");

include("models/cls_geneology.php");

$geneology = new Geneology();
$graph_data = $geneology->generate_graph_data();
$graph_data_until_end = $geneology->generate_graph_data_until_end();
$textual_summary = $geneology->getGeneologyTextualSummary($graph_data_until_end);

/*

$my_details = $user->get_current_user_details();
$my_downlines = $geneology->get_my_data();

print_r($my_details);
print_r($my_downlines);

$my_geneology = array('acc_type'=>$my_details['account_type']);
foreach($my_geneology as $me){
	//$me['left_child']
	$my_downlines

	//$me['right_child']
}
exit();

*/



include("views/admin/geneology.php");

function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Geneology.listener($);
			<?php if(isset($_GET['compose-message'])) : ?>
			Message.showcomposetips($);
			<?php endif; ?>
			Message.show_compose($);
			Message.send_message($);

		}); })(window, jQuery);
		var Message = {
			showcomposetips : function(){				
				Modal.hasButton = false;
				Modal._title = "Messaging Tips";
				Modal.addId = "messaging-tips";
				Modal.contents = 
					'<h1>Did you know?</h1>'+
					'<p>In order to message your downlines, the first thing you need to do is to hover the person you want to message. After hovering, the message button will appear. You just need to click the message button in order for you to type your message to the person. </p>'+
					'<img src="<?php echo SITE_URL ?>/assets/img/composetips.png" />';
				Modal.show($);
			},
			show_compose : function($){
				$(".btn-compose-message").on('click',function(){
					var afid = $(this).parent().parent().data('afid');
					Modal.hasHeader = 
					Modal.hasButton = false;
					Modal.addId = "compose_message_modal";
					Modal.contents = 
						'<form role="form">' +
							'<input type="hidden" name="toid" id="toid" value="'+afid+'" />'+
							'<div class="form-group">'+
								'<label for="">Messages:</label>'+
								'<textarea class="form-control" placeholder="" name="message"></textarea>'+
							'</div>'+
							'<input type="submit" class="btn btn-primary" value="Submit Message" />'+
						'</form>'
					Modal.show($);
				})				
			},
			send_message : function($){
				$(document).on('submit',"#compose_message_modal form",function(e){
					e.preventDefault();
					show_loader($);
					var _data = "a=sendmessage&"+$(this).serialize();
					$.post(window.location.href,_data, function(data){
						data = $.trim(data);
						if(data==1){								
							alert('Message successfully sent!');
							window.location.href="<?php echo SITE_URL ?>/messages";
						}
						else{
							alert('An error occured! Please contact the site administrator.');
						}
						close_loader($);
						console.log(data);
					});
					return false;
				})
			}

		}
		var Geneology = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$("#btn-textual").on('click',function(){
					that.show_textual(this);
					return false;
				})
				$("#btn-graphical").on('click',function(){
					that.show_graphical(this);
					return false;
				})
				$("#btn-search").on('click',function(){
					that.show_search(this);
					return false;
				})
				$("#btn-summary").on('click',function(){
					that.show_summary(this);
					return false;
				})
				that.setpopupcontrol();			

			},
			show_textual : function(that){
				$("#graphical").removeClass('active');
				$("#textual").addClass('active');
			},
			show_graphical : function(that){
				$("#graphical").addClass('active');
				$("#textual").removeClass('active');
			},
			show_search : function(that){
				Modal.hasHeader = 
				Modal.hasButton = false;
				Modal.addId = "search_geneology_modal";
				Modal.contents = 
					'<form role="form">' +
						'<div class="form-group">'+
							'<label for="">Date From:</label>'+
							'<input type="text" class="form-control" placeholder="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Date To:</label>'+
							'<input type="text" class="form-control" placeholder="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">ID Number:</label>'+
							'<input type="text" class="form-control" placeholder="" value="04272782" disabled />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Account Name:</label>'+
							'<input type="text" class="form-control" placeholder="" value="GIGER, KURT ROY RUIZO" disabled />'+
						'</div>'+
						'<a class="btn btn-primary" href="#">Search</a>'+
					'</form>'
				Modal.show($);
			},
			show_summary : function(that){
				Modal.hasHeader = 
				Modal.hasButton = false;
				Modal.addId = "summary_geneology_modal";
				Modal.contents = 
					'<table class="table table-striped" id="textual-data">' +
					'	<thead>' +
					'		<tr>'+
					'			<th>&nbsp;</th>'+
					'			<th>Paid</th>'+
					'			<th>FS</th>'+
					'			<th>Total Heads</th>'+
					'		</tr>'+
					'	</thead>'+
					'	<tbody>'+
					'		<tr>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'		</tr>'+
					'		<tr>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'		</tr>'+
					'	</tbody>'+
					'</table>';
				Modal.show($);
			},
			setpopupcontrol : function(){
				console.log('test');
				$("#graphical-data .user").each(function(){
					console.log(this);
					var _name = $(this).data('fullname');
					var afid = $(this).data('afid');
					var data = 	'<div class="popup-control">'+
								'<a href="#" class="btn-default btn btn-compose-message">Message</a>'+
								'<div class="note"><strong>'+_name+'</strong><br />'+afid+'</div>'+
								'</div>';
					$(this).prepend(data);
				});
			}
		}		
	</script>
	<?php	
}
Func::footer_hook('script');
include("views/partials/admin_footer.php");