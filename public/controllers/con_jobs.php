<?php
session_start();
Func::inactive_blocker();

include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");
include("views/admin/jobs.php");
function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Jobs.listener($);
		}); })(window, jQuery);
		var Jobs = {
			listener : function($){
				$("#jobs table a").on('click',function(){

					var thumb = $(this).find('img').attr('src');
					var title = $(this).data('title');
					var excerpt =$(this).data('excerpt');
					var filesrc= $(this).data('link');

					Modal._title = "Job Detail";
					Modal.hasHeader = true;
					Modal.hasButton = false;
					Modal.addId = "job_detail_modal";
					Modal.contents = $( "#"+ $(this).data('content') ).html()
					Modal.show($);

					$("#job_detail_modal").on('hidden.bs.modal', function (e) {
						$("#job_detail_modal").remove();
					})

				});
			}
		}
	</script> <?php
} 
Func::footer_hook('script');
include("views/partials/admin_footer.php");