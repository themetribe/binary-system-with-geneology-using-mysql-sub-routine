(function(global, $){ $(document).ready(function(){
	$('.datepicker').datepicker();

	buttons.bounce($);
	buttons.scroll($);
	gallery.grid($);
}); })(window, jQuery);


var buttons = {
	bounce : function($){
		$("#btn_get-started").on('click',function(){
			$(this).fadeOut('slow');
			$('#menu').show();
			$('#menu').addClass('animated bounceInDown');
		})
	},
	scroll : function($){
		$('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		})
	}
}

var gallery = {
	grid : function($){
		$( '#ri-grid' ).gridrotator( {
			rows		: 3,
			columns		: 15,
			animType	: 'fadeInOut',
			animSpeed	: 1000,
			interval	: 600,
			step		: 1,
			w320		: {
				rows	: 3,
				columns	: 4
			},
			w240		: {
				rows	: 3,
				columns	: 4
			}
		});
	}
}


