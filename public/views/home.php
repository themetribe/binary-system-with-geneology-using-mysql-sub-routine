<div class="block hero-full full-height Vcenter">
	<div class="container">
		<img src="<?php echo SITE_URL  ?>/assets/img/logo.jpg" class="img-circle">
		<h1>INLIGHT<br /> MARKETING</h1>
		<p class="sub-heading">A clear path to wealth and abundance.</p>
		<button class="btn btn-default" id="btn_get-started">Get Started</button>
		
		<ul id="menu" class="menu">
			<img class="logo-blue" src="<?php echo SITE_URL ?>/assets/img/logo-blue.png">
			<li><a class="btn btn-default" href="#about-us">About Us</a></li>
			<li><a class="btn btn-default" href="#product">Products</a></li>
			<li><a class="btn btn-default" href="#service">Services</a></li>
			<li><a class="btn btn-default" href="<?php echo SITE_URL ?>/join">Opportunity</a></li>
			<li><a class="btn btn-default" href="#affiliates">Affiliates</a></li>
			<li><a class="btn btn-default" href="<?php echo SITE_URL ?>/investors">Investors</a></li>
			<li><a class="btn btn-default" href="#support">Support</a></li>
			<a id="btnlogin" class="btn btn-default" href="#">Member Login</a>
		</ul>
	</div>      
</div>


<section id="about-us" class="block about-us">
	<div class="container">
		<h1>About Inlight Marketing</h1>
		<p>Inlight Unlimited is focusing on the fundamentals of human development and behavior that most affect personal, profession and business performance.</p>
		
		<div class="row" id="mission_vision">
			<div class="col-md-4">
				<h3>Our Mission</h3>
				<p>To shine and to give light to the people. To inspire and motivate through Inlight's outstanding eBooks, services, coaching, trainings and opportunities.
				To deliver wisdom, knowledge and right perspective by duplicating oneself to another. To enlighten the people and let the world know we have a better way.</p>
			</div>
			<div class="col-md-3">
				<h3>Our Vision</h3>
				<p>To change the outlook of every member positively into their lives and careers. A transformed network and duplicated individuals that are inspired and motivated to help other people create a lifestyle of wealth and abundance. </p>
			</div>
			<div class="col-md-5">
				<h3>Our Standards</h3>
				<ul>
					<li><strong>S</strong>ee your goal.</li>
					<li><strong>U</strong>nderstand the obstacles.</li>
					<li><strong>C</strong>reate a positive solution and right perspective.</li>
					<li><strong>C</strong>lear your mind of self distraction, fear and doubt.</li>
					<li><strong>E</strong>mbrace every challenge.</li>
					<li><strong>S</strong>tay on the right path and stay focus.</li>
					<li><strong>S</strong>how the world that you can do it!</li>
				</ul>
			</div>
		</div>

		<div id="founders">

			<div class="row author">
				<div class="col-md-4">
					<img src="<?php echo SITE_URL ?>/assets/img/Kurt.jpg" />
				</div>
				<div class="col-md-8">
					<h3>Kurt Roy Giger</h3>
					<h4>President</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sit amet lacinia ipsum, vitae accumsan nulla. Mauris id sodales diam. Maecenas pellentesque mattis felis sed cursus. Duis et nulla ultricies, tempor metus ac, cursus justo. Vestibulum cursus faucibus ipsum. Suspendisse vitae mi ac justo tristique facilisis. Vivamus dignissim nec quam sit amet gravida. Donec at lectus neque.</p>
				</div>				
			</div>

			<div class="row author">
				<div class="col-md-8 right">
					<h3>Jimmy Giger Jr.</h3>
					<h4>Head Pioneer Associate</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sit amet lacinia ipsum, vitae accumsan nulla. Mauris id sodales diam. Maecenas pellentesque mattis felis sed cursus. Duis et nulla ultricies, tempor metus ac, cursus justo. Vestibulum cursus faucibus ipsum. Suspendisse vitae mi ac justo tristique facilisis. Vivamus dignissim nec quam sit amet gravida. Donec at lectus neque.</p>
				</div>
				<div class="col-md-4">
					<img src="<?php echo SITE_URL ?>/assets/img/Jr.jpg" />
				</div>
			</div>			

			<div class="row author">
				<div class="col-md-4">
					<img src="<?php echo SITE_URL ?>/assets/img/Francis.jpg" />
				</div>
				<div class="col-md-8">
					<h3>Francis Albores</h3>
					<h4>Admin Associate</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sit amet lacinia ipsum, vitae accumsan nulla. Mauris id sodales diam. Maecenas pellentesque mattis felis sed cursus. Duis et nulla ultricies, tempor metus ac, cursus justo. Vestibulum cursus faucibus ipsum. Suspendisse vitae mi ac justo tristique facilisis. Vivamus dignissim nec quam sit amet gravida. Donec at lectus neque.</p>
				</div>				
			</div>

		</div>

		
	</div>
</section>


<section id="product" class="block product">
	<div class="container">
		<h1>Products</h1>
		<div class="row">
			<div class="col-md-12">
				<p>Inlight Marketing main products are eBooks, Audios and Videos. We are offering you the ultimate collections of high value, unique and competitive products of all time and with the best learning system you could possibly get. We make sure that what you get in our company will give you something that no money can buy. An extra ordinary learning will take place within you that will awaken your maximum potential of becoming a successful person.</p>
				<img src="<?php echo SITE_URL ?>/assets/img/thumbnail-default-large.jpg">
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<p>Our products are organized and categorized depending on your package level. We prepared a unique step by step learning system that you can follow. It will guide you from start to finish by selecting which career path you will take.</p>
			</div>
			<div class="col-md-6">
				<img src="<?php echo SITE_URL ?>/assets/img/thumbnail-default-medium.jpg">
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="<?php echo SITE_URL ?>/assets/img/thumbnail-default-medium.jpg">
			</div>
			<div class="col-md-6">
				<p>We offer you an additional advanced modules and handbooks once you are in the premier level.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<p>And the most exciting privilege you could get as a member is the trainings, seminars and webinar that we provide, which covers the topic on how to become successful in 5 aspects of your life; Spiritual, Financial, Intellectual, Physical and Relational. And it covers modulated topics of each aspect; like businesses, entrepreneurs, personal development, internet marketing, network marketing, concept, strategies and many more. All this will be given for free once you become an affiliate in our company. And we assure you that your stay in our company will be worthwhile. </p>
			</div>
			<div class="col-md-6">
				<img src="<?php echo SITE_URL ?>/assets/img/thumbnail-default-medium.jpg">
			</div>
		</div>
	</div>
</section>


<section id="service" class="block service">
	<div class="container">
		<h1>Services</h1>
		<div class="row">
			<div class="col-md-3">
				<i class="fa fa-circle-o-notch"></i>
				<h4>Automated Marketing Funnel</h4>
					<p>Automated sales and marketing funnel design to help you make more money 24/7.</p>
			</div>	
			<div class="col-md-3">
				<i class="fa fa-play-circle-o"></i>
				<h4>Done For You Sales Videos</h4>
					<p>Not good in selling? No problem! Let us do the hard work for you.</p>
			</div>		
			<div class="col-md-3">
				<i class="fa fa-envelope"></i>
				<h4>Integrated Follow Up System</h4>
				<p>We help you make more sales using our integrated and automated email follow up system.</p>
			</div>	
			<div class="col-md-3">
				<i class="fa fa-coffee"></i>
				<h4>Commission Revolution</h4>
				<p>We pay the highest percentage of affiliate commission out there.</p>
			</div>	
		</div>
		<br />
		<div class="row">
			<div class="col-md-3">
				<i class="fa fa-users"></i>
				<h4>Automated Marketing Funnel</h4>
					<p>Automated sales and marketing funnel design to help you make more money 24/7.</p>
			</div>	
			<div class="col-md-3">
				<i class="fa fa-desktop"></i>
				<h4>Done For You Sales Videos</h4>
					<p>Not good in selling? No problem! Let us do the hard work for you.</p>
			</div>		
			<div class="col-md-3">
				<i class="fa fa-life-ring"></i>
				<h4>Email Chat Support</h4>
				<p>We help you make more sales using our integrated and automated email follow up system.</p>
			</div>	
			<div class="col-md-3">
				<i class="fa fa-calendar-o"></i>
				<h4>Life Changing Events</h4>
				<p>We pay the highest percentage of affiliate commission out there.</p>
			</div>	
		</div>
	</div>
</section>

<section id="affiliates" class="block investor">
	<div class="container">
		<h1>Affiliates</h1>
		<p>Sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.</p>
	
		<div id="ri-grid" class="ri-grid ri-grid-size-2">
			<ul>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/1.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/2.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/3.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/4.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/5.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/6.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/7.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/8.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/9.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/10.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/11.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/12.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/13.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/14.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/15.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/16.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/17.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/18.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/19.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/20.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/21.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/22.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/23.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/24.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/25.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/26.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/27.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/28.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/29.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/30.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/31.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/32.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/33.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/34.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/35.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/36.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/37.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/38.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/39.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/40.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/41.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/42.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/43.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/44.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/45.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/46.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/47.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/48.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/49.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/50.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/51.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/52.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/53.jpg"/></a></li>
				<li><a href="#"><img src="<?php echo SITE_URL ?>/assets/img/medium/54.jpg"/></a></li>
			</ul>
		</div>
	</div>
</section>


<section id="support" class="block support">
	<div class="container">
		<h1>Support</h1>
		<!-- <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam</p> -->
		<div class="wrapper">
			<?php include 'views/admin/support.php'; ?>
		</div>
	</div>
</section>

<section id="footer">
	<a href="/legalities">Legalities</a> | <a href="/tandc">Terms and Conditions</a>

</section>