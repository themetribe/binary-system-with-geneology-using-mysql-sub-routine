<div id="profile-wrapper" class="main">
	<div id="profile">
		<div class="top clearfix">
			<div id="user-details" class="pull-left">
				<img src="<?php echo SITE_URL ?>/assets/img/avatar.jpg" />				
			</div>
			<div id="network" class="clearfix">
				<h2 id="user-name">Francis M Albores</h2>
				<a href="#" class="btn btn-default">Send Message</a>
				<hr style="clear:both;" />
				<div class="avatar">
					<span class="ribbon">Upline</span>
					<a href="#"><img src="<?php echo SITE_URL ?>/assets/img/avatar.jpg" /></a>
				</div>
				<div class="avatar">
					<span class="ribbon">Downline</span>
					<a href="#"><img src="<?php echo SITE_URL ?>/assets/img/avatar.jpg" /></a>
				</div>
				<div class="avatar">
					<span class="ribbon">Crossline</span>
					<a href="#"><img src="<?php echo SITE_URL ?>/assets/img/avatar.jpg" /></a>
				</div>
			</div>		
		</div>
		<div class="below clearfix">
			<div class="sidebar">
				<div id="user-meta">				
					<p>Lives in <strong>Davao City</strong></p>
					<p>Some info here</p>
					<p>Some info here</p>
				</div>
			</div>
			<div class="content">
				<form role="form">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-pencil"></i></div>
							<textarea class="form-control" placeholder="Write something..."></textarea>
							<div class="input-group-addon">
							<button type="submit" class="btn btn-default">Post</button>
							</div>
						</div>
					</div>
				</form>
				<hr />
				<div id="comments">
					<div class="comment">
						<div class="comment-meta clearfix vcenter">
							<div class="comment-avatar">
								<img src="<?php echo SITE_URL ?>/assets/img/avatar.jpg" />
							</div>						
							<div class="comment-name">Joe Smith</div>
							<div class="comment-date">September 5, 2014</div>
						</div>
						<div class="comment-body">
							Lorem Ipsum dolor sit amer
							<img src="<?php echo SITE_URL ?>/assets/img/comment-image-sample.jpg" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>