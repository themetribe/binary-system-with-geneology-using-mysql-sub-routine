<div id="messages" class="main">

	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li class="active"><a href="#0" role="tab" data-toggle="tab">Inbox</a></li>
		<li><a href="#1" role="tab" data-toggle="tab">Sent</a></li>
		<?php /*  HIDE FOR NOW
		<li><a href="#2" role="tab" data-toggle="tab">Trash</a></li>
		*/ ?>
		<a id="btn-compose-message" href="<?php echo SITE_URL ?>/geneology?compose-message=1" class="btn btn-default">Compose</a>
	</ul>


	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane fade in active" id="0">			
			<table class="table table-striped">
				<thead>
					<tr>
						<th>From</th>
						<th>Messages</th>
						<th>Date/Time</th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($inbox)>0) : 
						foreach($inbox as $in) : ?>
						<tr>
							<td class="from"><?php echo $in['_from'] ?></td>
							<td><a href="#" class="view-details" data-fullmessage="<?php echo $in['message'] ?>"><?php echo substr($in['message'],0,5); ?>&hellip;</a></td>
							<td><?php echo $in['date_time'] ?></td>
						</tr>
					<?php 
						endforeach;
					else: ?>
						<tr><td colspan="3">No record here</td></tr>
					<?php endif; ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				</tfoot>
			</table>
			<?php /* HIDE FOR NOW
			<a href="#" class="btn btn-default pull-left"><i class="fa fa-angle-left"></i> Prev</a>
			<a href="#" class="btn btn-default pull-right">Next <i class="fa fa-angle-right"></i></a>
			*/ ?>
		</div>
		<div class="tab-pane fade" id="1">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>To</th>
						<th>Messages</th>
						<th>Date/Time</th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($sent)>0) : 
						foreach($sent as $se) : ?>
						<tr>
							<td><?php echo $se['_to'] ?></td>
							<td><?php echo $se['message'] ?></td>
							<td><?php echo $se['date_time'] ?></td>
						</tr>
					<?php 
						endforeach;
					else: ?>
						<tr><td colspan="3">No record here</td></tr>
					<?php endif; ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				</tfoot>
			</table>
			<?php /* HIDE FOR NOW
			<a href="#" class="btn btn-default pull-left"><i class="fa fa-angle-left"></i> Prev</a>
			<a href="#" class="btn btn-default pull-right">Next <i class="fa fa-angle-right"></i></a>
			*/ ?>
		</div>
		<div class="tab-pane fade" id="2">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>To</th>
						<th>Messages</th>
						<th>Date/Time</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($trash)>0 && $trash!="") : 
						foreach($trash as $tr) : ?>
						<tr>
							<td><?php echo $tr['_to'] ?></td>
							<td><?php echo $tr['message'] ?></td>
							<td><?php echo $tr['date_time'] ?></td>
						</tr>
					<?php 
						endforeach;
					else: ?>
						<tr><td colspan="3">No record here</td></tr>
					<?php endif; ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				</tfoot>
			</table>
			<?php /* HIDE FOR NOW 
			<a href="#" class="btn btn-default pull-left"><i class="fa fa-angle-left"></i> Prev</a>
			<a href="#" class="btn btn-default pull-right">Next <i class="fa fa-angle-right"></i></a>
			*/ ?>
		</div>
		<div class="tab-pane fade" id="3">...</div>
	</div>

</div>