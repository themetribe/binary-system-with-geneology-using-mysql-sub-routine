<div id="geneology" class="main">

	<div id="geneology-mode" class="clearfix">
		<a href="#" id="btn-graphical" class="btn-toggle active center"><i class="fa fa-columns"></i> Graphical</a>
		<a href="#" id="btn-textual" class="btn-toggle center"><i class="fa fa-th-list"></i> Textual</a>
	</div>
	
	<div class="clearfix active" id="graphical">

		<?php
		/* WILL BE USED LATER
		<div class="pull-left" >
			<a href="#"><i class="fa fa-angle-double-up"></i> Top</a>
			<a href="#"><i class="fa fa-angle-up"></i> Up</a>
		</div> */ ?>

		<div id="legend">
			<strong>Legend</strong>
			<ul>
				<li class="basic"><span></span> - Basic Account</li>
				<li class="premier"><span></span> - Premier Account</li>
				<li class="silver"><span></span> - Silver Investor</li>
				<li class="gold"><span></span> - Gold Investor</li>
				<li class="diamond"><span></span> - Diamond Investor</li>
			</ul>
			<ul>
				<li class="direct"><span></span> - Direct Referral</li>
				<li class="indirect"><span></span> - Indirect Referral</li>
			</ul>
		</div>
		
		<div id="graphical-data">
			<div class="stage stage-1">
				<?php $geneology->generate_graph_html($graph_data,0,0); ?>				
				<div class="stage stage-2">
					<div class="left">
						<?php $geneology->generate_graph_html($graph_data,1,0); ?>	
						<div class="stage stage-3">
							<div class="left">
								<?php $geneology->generate_graph_html($graph_data,2,0); ?>	
								<div class="stage stage-4">
									<div class="left">
										<?php $geneology->generate_graph_html($graph_data,3,0); ?>	
									</div>
									<div class="right">
										<?php $geneology->generate_graph_html($graph_data,3,1); ?>	
									</div>
								</div>
							</div>
							<div class="right">
								<?php $geneology->generate_graph_html($graph_data,2,1); ?>	
								<div class="stage stage-4">
									<div class="left">
										<?php $geneology->generate_graph_html($graph_data,3,2); ?>	
									</div>
									<div class="right">
										<?php $geneology->generate_graph_html($graph_data,3,3); ?>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="right">
						<?php $geneology->generate_graph_html($graph_data,1,1); ?>	
						<div class="stage stage-3">
							<div class="left">
								<?php $geneology->generate_graph_html($graph_data,2,2); ?>	
								<div class="stage stage-4">
									<div class="left">
										<?php $geneology->generate_graph_html($graph_data,3,4); ?>	
									</div>
									<div class="right">
										<?php $geneology->generate_graph_html($graph_data,3,5); ?>	
									</div>
								</div>
							</div>
							<div class="right">
								<?php $geneology->generate_graph_html($graph_data,2,3); ?>	
								<div class="stage stage-4">
									<div class="left">
										<?php $geneology->generate_graph_html($graph_data,3,6); ?>	
									</div>
									<div class="right">
										<?php $geneology->generate_graph_html($graph_data,3,7); ?>	
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php /*<div id="graphical-data">
			<div class="stage stage-1">
				<div class="user referral-direct">
					<i class="sprite user-premier avatar-default"></i>
				</div>
				<div class="connector">
					<div class="left unhealthy">
						<span class="l1"></span>
						<span class="l2"></span>
						<span class="l3"></span>
					</div>
					<div class="right healthy">
						<span class="l1"></span>
						<span class="l2"></span>
						<span class="l3"></span>
					</div>
				</div>
				<div class="stage stage-2">
					<div class="left">
						<a href="#" class="user referral-indirect">
							<i class="sprite user-basic avatar-default"></i>
							
						</a>						
						<div class="connector">
							<div class="left">
								<span class="l1"></span>
								<span class="l2"></span>
								<span class="l3"></span>
							</div>
							<div class="right">
								<span class="l1"></span>
								<span class="l2"></span>
								<span class="l3"></span>
							</div>
						</div>
						<div class="stage stage-3">
							<div class="left">
								<a href="#" class="user">
									<i class="sprite avatar-default-sm"></i>
								</a>
							</div>
							<div class="right">
								<a href="#" class="user">
									<i class="sprite avatar-default-sm"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="right">
						<a href="#" class="user referral-direct">
							<i class="sprite user-premier avatar-default"></i>
						</a>
						<div class="connector">
							<div class="left healthy">
								<span class="l1"></span>
								<span class="l2"></span>
								<span class="l3"></span>
							</div>
							<div class="right healthy">
								<span class="l1"></span>
								<span class="l2"></span>
								<span class="l3"></span>
							</div>
						</div>
						<div class="stage stage-3">
							<div class="left">
								<a href="#" class="user referral-indirect">
									<i class="sprite user-basic-small avatar-default-sm"></i>
								</a>
							</div>
							<div class="right">
								<a href="#" class="user referral-direct">
									<i class="sprite user-basic-small avatar-default-sm"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		*/ ?>
	</div>	

	<div class="clearfix" id="textual">
		<?php /* HIDE FOR NOW
		<div class="buttons">
			
			<a href="#" id="btn-search" class="btn btn-default"><i class="fa fa-search"></i> Search</a>
			
			<a href="#" id="btn-summary" class="btn btn-default"><i class="fa fa-calculator"></i> Summary</a>
		</div>
		*/ ?>
		<div class="alert alert-warning fade in" role="alert">
			<strong>Summary</strong>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Total # of Downlines</th>
						<th>Personal Affiliate</th>
						<th>Total # of Indirects</th>
						<th>Total # of Left</th>
						<th>Total # of Right</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $textual_summary['total_downlines'] ?></td>
						<td><?php echo $textual_summary['total_direct'] ?></td>
						<td><?php echo $textual_summary['total_indirect'] ?></td>
						<td><?php echo $textual_summary['total_left'] ?></td>
						<td><?php echo $textual_summary['total_right'] ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<?php 
		//print_r($graph_data_u);
		?>
		<table class="table table-striped" id="textual-data">
			<thead>
				<tr>
					<th>Level</th>
					<th>Upline</th>
					<th>ID No</th>
					<th>Name</th>
					<th>Group</th>
					<th>Side</th>
					<th>RegDate</th>
					<th>Upgrade Date</th>
					<th>Sponsor</th>
					<th>Account Type</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if(count($graph_data_until_end)>0) : ?>
					<?php foreach($graph_data_until_end as $key=>$val) : ?>
						<?php foreach($val as $k=>$v) : 
							if($v!=-1) : ?>
								<tr>
									<td><?php echo $key ?></td>
									<td><?php echo $v['upline_id'] ?></td>
									<td><?php echo $v['ID'] ?></td>
									<td><?php echo $v['fullname'] ?></td>
									<td><?php echo $v['group'] ?></td>
									<td><?php echo $v['side'] ?></td>
									<td><?php echo $v['registered_date'] ?></td>
									<td><?php echo $v['upgrade_date'] ?></td>
									<td><?php echo $v['sponsor_ID'] ?></td>
									<td><?php echo $geneology->getAccountTypeIcon($v['account_type']) ?></td>
								</tr>
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endforeach; ?>
				<?php else : ?>
					<tr>
						<td colspan="10">No data for now</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>

	</div>	

</div>