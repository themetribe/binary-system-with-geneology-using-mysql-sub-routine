<div id="myaccount" class="main">

<div class="block" id="main-buttons">
	<a href="#" id="btn-personal" class="btn-toggle active center"><i class="fa fa-columns"></i> Personal Info</a>
	<a href="#" id="btn-security" class="btn-toggle center"><i class="fa fa-th-list"></i> Security</a>
	<a href="#" id="btn-activation" class="btn-toggle center"><i class="fa fa-plug"></i> Activation</a>
	<?php if($_SESSION['user_status']!=1){ ?>
	<a href="#" id="btn-payment" class="btn-toggle center"><i class="fa fa-cc-mastercard"></i> Payment</a>
	<?php } ?>
</div>

<div class="wrapper">
	
	<div class="block row contents active" id="personal-info-cont">
		<form role="form" action="" method="post" enctype="multipart/form-data">
			<div class="col-md-9">
				
				<input type="hidden" name="a" value="personal-info" />

				<div class="form-group">
					<label for="">Username:</label>
					<input type="text" class="form-control" name="username" placeholder="" value="<?php echo $usr_details['username']; ?>" />
				</div>
				<div class="row">
					<div class="form-group col-md-4">
						<label for="">First Name:</label>
						<input type="text" class="form-control" name="fname" placeholder="" value="<?php echo $usr_details['fname']; ?>" />
					</div>
					<div class="form-group col-md-4">
						<label for="">Middle Name:</label>
						<input type="text" class="form-control" name="mname" placeholder="" value="<?php echo $usr_details['mname']; ?>" />
					</div>
					<div class="form-group col-md-4">
						<label for="">Last Name:</label>
						<input type="text" class="form-control" name="lname" placeholder="" value="<?php echo $usr_details['lname']; ?>" />
					</div>
				</div>

				<div class="form-group">
					<label for="">Complete Address:</label>
					<textarea class="form-control" name="address" placeholder="Please enter complete address"><?php echo $usr_details['address']; ?></textarea>
				</div>
				
				<div class="row">
					<div class="form-group col-md-6">
						<label for="">City:</label>
						<input type="text" class="form-control" placeholder="Please enter city" name="city" value="<?php echo $usr_details['city']; ?>" />
					</div>
					<div class="form-group col-md-6">
						<label for="">Province:</label>
						<input type="text" class="form-control" placeholder="Please enter province" name="province" value="<?php echo $usr_details['province']; ?>"/>
					</div>	
				</div>
				
				<div class="row">
					<div class="form-group col-md-4">
						<label for="">Birth Date:</label>
						<input type="text" class="form-control" placeholder="DD/MM/YY" name="bdate" value="<?php echo $usr_details['bdate']; ?>" />
					</div>
					<div class="form-group col-md-4">
						<label for="">Gender:</label>
						<input type="text" class="form-control" placeholder="Please enter gender" name="gender" value="<?php echo $usr_details['gender']; ?>"/>
					</div>
					<div class="form-group col-md-4">
						<label for="">Status:</label>
						<input type="text" class="form-control" name="personal_status" placeholder="Please enter satus" value="<?php echo $usr_details['personal_status'] ?>"/>
					</div>	
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label for="">Email:</label>
						<input type="email" class="form-control" name="email" placeholder="Please enter email" value="<?php echo $usr_details['email']; ?>"/>
					</div>
					<div class="form-group col-md-6">
						<label for="">Phone:</label>
						<input type="text" class="form-control" name="phone" placeholder="xxx-xxxx-xxxx" value="<?php echo $usr_details['phone']; ?>"/>
					</div>
				</div>
				<?php /* HIDE FOR NOW, WILL BE USED IN THE FUTURE
				<hr />

				<div class="form-group">
					<label for="">Payout Type:</label>
					<select class="form-control">
						<option value="" disabled>[Choose Payout Type]</option>
						<option>Bank</option>
						<option>Luwelyer</option>
					</select>			
				</div>
				<div class="form-group">
					<label for="">Account Name:</label>
					<input type="text" class="form-control" placeholder="Please enter account name" />
				</div>
				<div class="form-group">
					<label for="">Account Number:</label>
					<input type="text" class="form-control" placeholder="Please enter account number" />
				</div>
				*/ ?>
				<input type="submit" class="btn btn-primary btn-lg" value="Save" />
			
			</div>
			<div class="col-md-3">
				<div class="block user-avatar">
					<strong><?php echo $_SESSION['user_name'].' - #'.$_SESSION['user_ID']; ?></strong>
					<img width="200" id="avatar-prev" src="<?php echo $user_avatar_src ?>" />
					<?php /*<img width="200" id="avatar-prev" src="<?php echo SITE_URL ?>/assets/img/upline.jpg" />*/ ?>
					<div class="sponsor-info">					
						<strong>Change Profile Picture</strong>
						<input type="file" id="change-avatar" name="change-avatar" />
					</div>				
				</div>
				<div class="block user-avatar">
					<h3>My Sponsor</h3>
					<img class="sponsor_avatar" src="<?php echo $sponsor_avatar_src ?>" />
					<div class="sponsor-info">
						<strong><?php echo $sponsor_details['fname'].' '.$sponsor_details['mname'].' '.$sponsor_details['lname'] ?></strong>
						<small><?php echo $sponsor_details['ID'] ?></small>
						<span class="num"><?php echo $sponsor_details['phone'] ?></span>
						<span class="email"><?php echo $sponsor_details['email'] ?></span>
					</div>				
				</div>
			</div>
		</form>
	</div>

	<div class="block contents" id="security-cont">
		
		
		<div class="row">
			<div class="col-md-6">
				<form id="frm_changepassword" role="form">
					<input type="hidden" name="a" value="change-password" />
					<h3>Change Password</h3>
					<div class="form-group">
						<label for="">Old Password:</label>
						<input type="password" class="form-control" name="old_password" placeholder="Please enter your old password" />
					</div>
					<div class="form-group">
						<label for="">New Password:</label>
						<input type="password" class="form-control" id="new_password" name="new_password" placeholder="Please enter your new password" />
					</div>
					<div class="form-group">
						<label for="">Retype New Password:</label>
						<input type="password" class="form-control" id="retype_new_password" name="retype_new_password" placeholder="Please retype your new password" />
					</div>
					<input type="submit" class="btn btn-default" value="Update Password" />
				</form>
			</div>
			<div class="col-md-6">
				<form id="frm_changepin" role="form">
					<input type="hidden" name="a" value="change-pin" />
					<h3>Change PIN</h3>
					<div class="form-group">
						<label for="">Old PIN:</label>
						<input type="password" class="form-control" name="old_pin" placeholder="Please enter your old password" />
					</div>
					<div class="form-group">
						<label for="">New PIN:</label>
						<input type="password" class="form-control" id="new_pin" name="new_pin" placeholder="Please enter your new password" />
					</div>
					<div class="form-group">
						<label for="">Retype New PIN:</label>
						<input type="password" class="form-control" id="retype_new_pin" name="retype_new_pin" placeholder="Please retype your new password" />
					</div>
					<input type="submit" class="btn btn-default" value="Update PIN" />
				</form>
			</div>
		</div>
	</div>

	<div class="block contents" id="activation-cont">
		<?php if($_SESSION['user_status']==0): ?>
			<h1><i class="fa fa-thumbs-o-down"></i></h1>
			<h3>Sorry you're account is still</h3>
			<h2 class="inactive">Inactive</h2>
		<?php else : ?>			
			<h3>Activation Codes</h3>
			<p>You can now use these following activation codes to register a new account instead of paying money.</p>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Code</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(count($activation_codes)>0 && $activation_codes!=false) : 
					foreach($activation_codes as $ac) : $status = ($ac['status']==0) ? "Used" : "Available" ; ?>
					<tr>
						<td><?php echo $ac['code'] ?></td>
						<td><?php echo $status ?></td>
					</tr>	
					<?php endforeach; else: ?>
					<tr>
						<td colspan="2">Sorry no activation code yet</td>
					</tr>
					<?php endif; ?>
				</tbody>
			</table>
			<hr />
			<h1><i class="fa fa-thumbs-o-up"></i></h1>
			<h3>Your account is </h3>
			<h2>Activated</h2>
		<?php endif; ?>
	</div>
	<?php 
	if($_SESSION['user_status']!=1){
		include('myaccount-payment.php');	
	}
	?>
</div>