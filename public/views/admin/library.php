<div class="main" id="library">

	<div class="block clearfix" id="menu">
		<ul class="nav navbar-nav">
			<li><a class="ebooks" href="#">Ebooks</a></li>
			<li><a class="video" href="#">Video</a></li>
			<li><a class="audio" href="#">Audio</a></li>
			<li><a class="mixed" href="<?php echo SITE_URL ?>/library">Mixed</a></li>
			<li class="dropdown">
				<a data-toggle="dropdown" href="#">Mentor <span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">					
					<li><a href="#">Jim Rohn</a></li>
					<li><a href="#">Kurt Giger</a></li>
					<li><a href="#">Robert Kiyosaki</a></li>
					<li><a href="#">Warren Buffet</a></li>
					<li><a href="#">Kris Aquino</a></li>
				</ul>
			</li>
		</ul>				
	</div>

	<div class="contents">
		<?php
		if($library!="" && count($library)>0){
			foreach($library as $key=>$val):
				$type = $cls_library->gettype($val['type']);
				?>

				<div class="item" data-link="<?php echo Func::get_secured_resources($type, $val['file_src'],0); ?>" data-title="<?php echo $val['title'] ?>" data-excerpt="<?php echo $val['excerpt'] ?>">
					<a href="#"></a>
					<span class="favorite center"><i class="fa fa-heart"></i></span>
					<img width="214" src="<?php echo Func::get_secured_resources($type.'thumb', $val['thumb_path'],0); ?>" />				
				</div>

				<?php
			endforeach;
		}
		else{
			echo "<center>Something's wrong. Can't fetch resources.</center>";
		}
		?>
	</div>
</div>