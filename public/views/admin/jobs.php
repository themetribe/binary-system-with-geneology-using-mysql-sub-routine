<div id="jobs" class="main">

	<h2>Jobs Available for You!</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse gravida urna urna.</p>

	<table class="table table-bordered">
		<tr>
			<td><a data-content="job-1" href="#">Project Manager / Graphic Artist</a></td>
		</tr>
		<tr>
			<td><a data-content="job-1" href="#">Project Manager / Graphic Artist</a></td>
		</tr>
		<tr>
			<td><a data-content="job-1" href="#">Project Manager / Graphic Artist</a></td>
		</tr>
		<tr>
			<td><a data-content="job-1" href="#">Project Manager / Graphic Artist</a></td>
		</tr>
		<tr>
			<td><a data-content="job-1" href="#">Project Manager / Graphic Artist</a></td>
		</tr>
		<tr>
			<td><a data-content="job-1" href="#">Project Manager / Graphic Artist</a></td>
		</tr>
	</table>

</div>

<div style="display:none;">
	<div id="job-1">
		<small class="pull-right">Post Date 26-Sep-14</small>
		<br style="clear:both;" />
		<img src="<?php echo SITE_URL ?>/assets/img/jobheading.jpg" style="width: 100%;" />

		<p><strong>The Equicom Group</strong> is a diversified conglomerate composed primarily of companies in information technology, healthcare and financial services.</p>
		<p>Some of the companies within the group include Equitable Computer Services, Inc. (ECS), one of the most progressive computer solutions provider in the Philippines, Maxicare Healthcare Corporation (Maxicare), the country’s leading health maintenance organization (HMO), MyHealth, the country’s first full-service ambulatory clinic with state of the art medical equipment and highly trained medical professionals, MetroDental, a leading provider of world-class dental care, Equicom Savings Bank Inc. (EqB), a duly licensed thrift bank and recipient of the BSP’s 2012 Pagtugon Award, and ALGO Leasing and Finance Inc., a non-bank non-quasi bank incorporated in June 2007.</p>
	</div>
</div>