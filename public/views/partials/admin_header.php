<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
if(!isset($_SESSION['user_ID'])){
	header('Location:'.SITE_URL);
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Inlight Ultd. | Dashboard</title>

		<!-- Bootstrap -->
		<link href="<?php echo SITE_URL ?>/assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<link href="<?php echo SITE_URL ?>/assets/css/admin-style.css" rel="stylesheet">

	</head>
	<body <?php Func::body_class("acctype-{$_SESSION['user_type']}") ?>>

	<div id="header" class="block clearfix">
		<div id="branding" class="center">
			<a href="<?php echo SITE_URL ?>"><img src="<?php echo SITE_URL ?>/assets/img/logo2.png" id="site-logo" alt="Inlight Unlimited" title="Inlight Unlimited" /></a>
			<i class="shadow"></i>
		</div>
		<div class="pull-right vcenter">
			<p class="slogan"><i class="fa fa-quote-left"></i>A clear path to wealth and abundance<i class="fa fa-quote-right"></i></p>
			<a href="#" id="avatar">
				<img src="<?php echo $_SESSION['user_avatar_src'] ?>" />
			</a>
			<span id="username">Welcome <?php echo $_SESSION['user_fname'] ?>!</span>
			<a href="<?php echo SITE_URL ?>?logout=1" id="btn-logout" class="center"><i class="fa fa-power-off"></i></a>
		</div>
	</div>