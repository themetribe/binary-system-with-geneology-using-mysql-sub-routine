<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
if(isset($_SESSION['user_ID'])){
	header('Location:'.SITE_URL.'/dashboard');
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Inlight</title>

		<!-- Bootstrap -->
		<link href="<?php echo SITE_URL ?>/assets/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<link href="<?php echo SITE_URL ?>/assets/css/jquery-grid-style.css" rel="stylesheet">
		<link href="<?php echo SITE_URL ?>/assets/css/animate.css" rel="stylesheet">
		<link href="<?php echo SITE_URL ?>/assets/css/site.css" rel="stylesheet">
	</head>
	<body>