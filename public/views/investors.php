

<div id="investors">

	<div class="block hero-full full-height Vcenter">
		<div class="container">
			<ul id="menu" class="menu animated bounceInDown has-skin" style="display: block;">
				<img class="logo-blue" src="http://inlight.dev/assets/img/logo-blue.png">
				<li><a class="" href="../#about-us">About Us</a></li>
				<li><a class="" href="../#product">Products</a></li>
				<li><a class="" href="../#service">Services</a></li>
				<li><a class="" href="../#opportunity">Opportunity</a></li>
				<li><a class="" href="../#affiliates">Affiliates</a></li>
				<li><a class="" href="http://inlight.dev/investors">Investors</a></li>
				<li><a class="" href="../#support">Support</a></li>
				<a id="btnlogin" class="" href="#">Member Login</a>
			</ul>
		</div>      
	</div>


	<div class="block container">
		<h1>Our Investors</h1>
		<hr />
		<div class="row">
			<div class="col-md-4 investor">
				<img src="assets/img/silver-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Silver</h3>
				<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet dapibus ultricies. Vestibulum ante ipsum primis in faucibus orci .</p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/gold-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Gold</h3>
				<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet dapibus ultricies. Vestibulum ante ipsum primis in faucibus orci .</p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/diamond-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Diamond</h3>
				<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet dapibus ultricies. Vestibulum ante ipsum primis in faucibus orci .</p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/silver-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Silver</h3>
				<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet dapibus ultricies. Vestibulum ante ipsum primis in faucibus orci .</p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/gold-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Gold</h3>
				<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet dapibus ultricies. Vestibulum ante ipsum primis in faucibus orci .</p>
			</div>

			<div class="col-md-4 investor">
				<img src="assets/img/diamond-investor.png" />
				<h2>Become an Investor</h2>
				<h3>Diamond</h3>
				<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquet dapibus ultricies. Vestibulum ante ipsum primis in faucibus orci .</p>
			</div>

		</div>
	</div>
</div>