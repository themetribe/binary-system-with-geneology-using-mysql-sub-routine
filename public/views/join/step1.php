<div id="join-step-1" class="join-containers active">
	<p>(Wait up to 10 seconds for the video to play and make sure to turn on your speaker)</p>
	<div class="monitor">
		<span class="shadow"></span>
		<img src="<?php echo SITE_URL ?>/assets/img/sample.png" />
	</div>
	<a href="#" class="btn btn-primary center">Yes I Want To Know How!</a>
</div>